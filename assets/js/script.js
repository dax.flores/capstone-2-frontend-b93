let loginLogoutIndex = document.querySelector("#loginLogoutIndex");
let registerOrProfileIndex = document.querySelector("#registerOrProfileIndex");
let coursesListOrInfoIndex = document.querySelector("#coursesListOrInfoIndex");


let userToken = localStorage.getItem("token");


if (!userToken) {

registerOrProfileIndex.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/register.html" class="nav-link"> Register </a>
			</li>

		`


coursesListOrInfoIndex.innerHTML =

		`
		<li class="nav-item">
				<a href="./pages/courses.html" class="nav-link"> Courses </a>
			</li>
		`

loginLogoutIndex.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/login.html" class="nav-link"> Log In </a>
			</li>
		`


} else if (localStorage.isAdmin == "true") {
	
	registerOrProfileIndex.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/profilePage.html" class="nav-link"> Profile </a>
			</li>
		`	
	

	coursesListOrInfoIndex.innerHTML =

		`
		<li class="nav-item">
				<a href="./pages/coursesViewAdmin.html" class="nav-link"> Courses Info </a>
			</li>
		`
		

	loginLogoutIndex.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Logout </a>
			</li>
		`
	

} else if (localStorage.isAdmin == "false")  { //isAdmin == "false"

	registerOrProfileIndex.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/profilePage.html" class="nav-link"> Profile </a>
			</li>
		`
	

	coursesListOrInfoIndex.innerHTML =
		`
		<li class="nav-item">
				<a href="./pages/courses.html" class="nav-link"> Courses</a>
			</li>
		`


	loginLogoutIndex.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Logout </a>
			</li>
		`

}

