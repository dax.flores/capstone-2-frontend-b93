let cardFooter;


fetch('http://powerful-mountain-69813.herokuapp.com/api/courses/')
.then(res => res.json())
.then(data => {

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let courseData;

	if(data.length < 1) {

		courseData = "No courses available"

	} else {



		courseData = data.map(course => {
						
			cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Select Course
						</a>
					`

			

			return (
				`
					<div class="col-md-6 my-3">
						<div class="card h-100" >
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

		// Replaces the comma's in the array with an empty string
		}).join("");
	}

	document.querySelector("#coursesContainer").innerHTML = courseData;
})