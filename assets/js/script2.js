let loginLogout = document.querySelector("#loginLogout");
let registerOrProfile = document.querySelector("#registerOrProfile");
let coursesListOrInfo = document.querySelector("#coursesListOrInfo");


let userToken = localStorage.getItem("token");


if (!userToken) {

registerOrProfile.innerHTML = 
		`
			<li class="nav-item">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`

coursesListOrInfo.innerHTML =

		`
		<li class="nav-item">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>
		`

loginLogout.innerHTML = 
		`
			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log In </a>
			</li>
		`




} else if (localStorage.isAdmin == "true") {
	
	registerOrProfile.innerHTML = 
		`
			<li class="nav-item">
				<a href="./profilePage.html" class="nav-link"> Profile </a>
			</li>
		`

	coursesListOrInfo.innerHTML =

		`
		<li class="nav-item">
				<a href="./coursesViewAdmin.html" class="nav-link"> Courses Info </a>
			</li>
		`

	loginLogout.innerHTML = 
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
		`


} else if (localStorage.isAdmin == "false")  { //isAdmin == "false"
	
	registerOrProfile.innerHTML = 
		`
			<li class="nav-item">
				<a href="./profilePage.html" class="nav-link"> Profile </a>
			</li>
		`

	coursesListOrInfo.innerHTML =
		`
		<li class="nav-item">
				<a href="./courses.html" class="nav-link"> Courses</a>
			</li>
		`

	loginLogout.innerHTML = 
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
		`

	
	
}

$('.search-button').click(function(){
  $(this).parent().toggleClass('open');
});