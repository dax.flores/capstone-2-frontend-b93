let token = localStorage.getItem('token');
let userId = localStorage.getItem('id');

let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let mobileNo = document.querySelector("#mobileNo");
let email = document.querySelector("#userEmail");

fetch('http://powerful-mountain-69813.herokuapp.com/api/users/details', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
	})
	.then(res => res.json())
	.then(data => {

	console.log(data);

	firstName.placeholder = data.firstName;
	lastName.placeholder = data.lastName;
	mobileNo.placeholder = data.mobileNo;
	email.placeholder = data.email;
	firstName.value = data.firstName;
	lastName.value = data.lastName;
	mobileNo.value = data.mobileNo;
	email.value = data.email;
	

})

document.querySelector("#updateUser").addEventListener( "submit", (e) => {

	e.preventDefault();

	let firstNameUpdate = firstName.value;
	let lastNameUpdate = lastName.value;
	let mobileNoUpdate = mobileNo.value;
	let emailUpdate = email.value;
	let userIdUpdate = userId


	fetch('http://powerful-mountain-69813.herokuapp.com/api/users', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
        	userId: userId,
            firstName: firstNameUpdate,
            lastName: lastNameUpdate,
            email: emailUpdate,
            mobileNo: mobileNoUpdate
            
        })
    })
    .then(res => res.json())
    .then(data => {
        
    	console.log(data);

    	if(data === true){

    		// Update course successful
    	    // Redirect to courses page
    	    window.location.replace("./profilePage.html");

    	}else{

    	    // Error in updating a course
    	    alert("something went wrong");

    	}

    })

})