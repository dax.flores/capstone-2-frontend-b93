let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let modalButton = document.querySelector("#adminButton");

if(adminUser == "true" || adminUser) {

	modalButton.innerHTML =
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">
				Add Course
			</a>
		</div>
	`

}

fetch('http://powerful-mountain-69813.herokuapp.com/api/courses/admin')
.then(res => res.json())
.then(data => {

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let courseData;

	if(data.length < 1) {

		courseData = "No courses available"

	} else {



		courseData = data.map(course => {
			
			if ( course.isActive === true ){

			console.log(course);


			cardFooter = 
					`
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Edit
						</a>
						<a href="./viewCourseStats.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block editButton">
							View
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">
							Disable Course
						</a>
					`
			} else {

			console.log(course);

			cardFooter = 
					`
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Edit
						</a>
						<a href="./viewCourseStats.html?courseId=${course._id}" value="${course._id}" class="btn btn-warning text-white btn-block editButton">
							View
						</a>
						<a href="./enableCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-secondary text-white btn-block deleteButton">
							Enable Course
						</a>
					`

			}
			

			return (
				`
					<div class="col-md-6 my-3">
						<div class="card h-100">
							<div class="card-body">
								<h5 class="card-title">
									${course.name}
								</h5>
								<p class="card-text text-left">
									${course.description}
								</p>
								<p class="card-text text-right">
									₱ ${course.price}
								</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`
			)

		// Replaces the comma's in the array with an empty string
		}).join("");
	}

	document.querySelector("#coursesContainer").innerHTML = courseData;
})