let params = new URLSearchParams(window.location.search);

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

let name = document.querySelector("#courseName");
let price = document.querySelector("#coursePrice");
let description = document.querySelector("#courseDescription");

fetch(`http://powerful-mountain-69813.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	// console.log(data);

	name.innerHTML = data.name;
	price.innerHTML = `₱ ${data.price}`;
	description.innerHTML = data.description;

// End of Course and Class Information 


let enrollees = document.querySelector("#enrollees");

data.enrollees.forEach(enrolleeData => {

	let enrolleesID = data.enrollees.map(el => {
	// console.log(el.userId);  // id list of course' enrollees
	})

	fetch(`http://powerful-mountain-69813.herokuapp.com/api/users/`, {
	})
	.then(res => res.json())
	.then(data => {

		// console.log(data);
		data.forEach(el => {

		if (el._id === enrolleeData.userId) {

		enrollees.innerHTML +=
		`
		<tr>
			
			<td data-title="Id Numbers">${enrolleeData.userId}</td>
			<td data-title="First Name">${el.firstName}</td>
			<td data-title="Last Name">${el.lastName}</td>
			<td data-title="Mobile Number">${el.mobileNo}</td>
			<td data-title="Email">${el.email}</td>
		</tr>
		`
		}
		});	
		
	})


})


})
