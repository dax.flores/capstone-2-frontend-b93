let token = localStorage.getItem('token');
let profileContainer = document.querySelector("#profileContainer");
let profileSummary = document.querySelector("#profSummary");
let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let email = document.querySelector("#email");
let mobileNo = document.querySelector("#mobileNo");


if(!token || token === null) {

	alert("Log In First");

	window.location.href="./login.html";
} else {

fetch('http://powerful-mountain-69813.herokuapp.com/api/users/details', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			}
	})
	.then(res => res.json())
	.then(data => {

	console.log(data);

	profileSummary.innerHTML =

					`<div id="profSummary" class="mt-3">
                      <h4>${data.firstName} ${data.lastName}</h4>
                      <p class="text-secondary mb-1">
						<h3 class="text-center"></p>
                      <h6 class="text-muted font-size-sm">ID Number ${data._id}</h6>
                      <button class="btn btn-outline-primary"><a href="./editProfile.html"</a>Edit Profile</button>
                    </div>
                    `
	
	profileContainer.innerHTML =
		`
		                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">First Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      ${data.firstName}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Last Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      ${data.lastName}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Mobile Number</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      ${data.mobileNo}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      ${data.email}
                    </div>
                  </div>
                  <div class="row gutters-sm">
                <div class="col-sm-6 mb-3">
                </div>
              </div>
              <hr>

              <h6 class="d-flex align-items-center mb-3"><i class="material-icons text-info mr-2">Classes:</h6>
                  <table id="enrolledCourses" class="table">
							<thead>
								<tr>
									<th> Course Name </th>
									<th>Enrolled On</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody >
							</tbody>
						</table> 


		`

		// console.log(dataEnrollmentsString.length);
		let courses = document.querySelector("#enrolledCourses");

		let enrollmentsArray = data.enrollments;

		enrollmentsArray.forEach(courseData => {

			fetch(`http://powerful-mountain-69813.herokuapp.com/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {

				courses.innerHTML += 
					  `<tr>
							<td>${data.name}</td>
							<td>${courseData.enrolledOn}</td>
							<td>${courseData.status}</td>
						</tr>

					`

		})
	
	})

  })
}
